const api_url = 'https://aloha-test.aloha.ar:2526/api';
// const api_url = 'http://localhost:8000';
const apiKey = "47409081";
let session;
let publisher;
let sessionData = {'sessionId': '', 'token': '', 'cliente': '', 'asesor': ''};

const div_error = document.getElementById('screen_error');
const div_welcome = document.getElementById('screen_welcome');
const div_videocall = document.getElementById('screen_videocall');
const btnStart = document.getElementById('btnStart');

const btnMic = document.getElementById('btnMic');
const btnVideo = document.getElementById('btnVideo');
const btnChat = document.getElementById('btnChat');
const btnEnd = document.getElementById('btnEnd');

let statusMic = true;
let statusVideo = true;
let statusChat = true;

const msgHistory = document.getElementById('chatHistory');
const txtChat = document.getElementById('txtChat');


btnMic.addEventListener('click', () => {
    statusMic = !statusMic;
    publisher.publishAudio(statusMic);
});

btnVideo.addEventListener('click', () => {
    statusVideo = !statusVideo;
    publisher.publishVideo(statusVideo);
});

btnEnd.addEventListener('click', () => {
    closeSession()
});


urlParam = function(name){
  var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
  if (results==null){
  return null;
  }
  else{
  return results[1] || 0;
  }
};

function handleError(error) {
  if (error) {
    console.log(error.message);
  }
}


function initializeSession() {
  session = OT.initSession(apiKey, sessionData.sessionId);

  div_welcome.classList.add('collapse');
  div_videocall.classList.remove('collapse');

  // Subscribe to a newly created stream
  session.on('streamCreated', function(event) {
    session.subscribe(event.stream, 'subscriber', {
      insertMode: 'append',
      width: '100%',
      height: '100%'
    }, handleError);
  });


  // Create a publisher
  publisher = OT.initPublisher('publisher', {
    insertMode: 'append',
    width: '100%',
    height: '100%'
  }, handleError);

  // Connect to the session
  session.connect(sessionData.token, function(error) {
    // If the connection is successful, publish to the session
    if (error) {
      handleError(error);
    } else {
      session.publish(publisher, handleError);
    }
  });

  session.on('signal:msg', function(event) {
    var msg = document.createElement('div');
    let msgSender;
    let msgClass;

    if (event.from.connectionId === session.connection.connectionId) {
      msgClass = 'chat-mine';
      msgSender = sessionData.cliente;
    } else {
      msgClass = 'chat-theirs';
      msgSender = event.data.sender;
    }

    msg.classList.add(msgClass);

    // Header
    const d = new Date();
    let min = (d.getMinutes()<10?'0':'') + d.getMinutes();
    let msgHeader = document.createElement('span');
    msgHeader.classList.add('chat-header');
    msgHeader.innerHTML = `${msgSender} - ${d.getHours()}:${min}`;

    let msgMessage = document.createElement('p');
    msgMessage.classList.add('chat-msg');
    msgMessage.innerText = event.data.msg;

    msg.appendChild(msgHeader);
    msg.appendChild(msgMessage);
    msgHistory.appendChild(msg);
    msg.scrollIntoView();
});  
}


txtChat.addEventListener('keyup', function(event) {
  if (event.keyCode === 13) {
    event.preventDefault();

    session.signal({
        type: 'msg',
        data: {'sender': sessionData.cliente, 'msg': txtChat.value}
      }, function(error) {
      if (error) {
        console.log('Error sending signal:', error.name, error.message);
      } else {
        txtChat.value = '';
      }
    });
  }
});


function startSession() {
  initializeSession();  
}

function finishSession() {
  location.href = "/finish.html";
}

function init() {
  const param_code = urlParam('code');
  const url = `${api_url}/${param_code}`;

  if (!param_code) {
    div_welcome.classList.add('collapse');
    div_error.classList.remove('collapse');
  }

  axios.get(url)
  .then(function(res) {
    if (res.status==200) {
      console.log(res);
      sessionData.sessionId = res.data.sessionId;
      sessionData.token = res.data.tokenCli;
      sessionData.cliente = res.data.cliente;
      sessionData.asesor = res.data.asesor;

      document.getElementById('clientName').innerText = sessionData.cliente;
      document.getElementById('asesorName').innerText = sessionData.asesor;


      btnStart.classList.remove('disabled');
    }
  })
  .catch(function(err) {
    console.log('Error obteniendo datos conexión ' + err);
    div_welcome.classList.add('collapse');
    div_error.classList.remove('collapse');
    document.getElementById('error_text').innerHTML = 'Ha ocurrido un error obteniendo los datos de la conexión';
  });
}

init();


# Comm-Server-Vonage
## _Create Virtual Environment and Activate_
```
$ python3 -m venv .venv
$ source .venv/bin/activate
```

## _Install Dependencies_
```
pip install -r requirements.txt
```

## _Create .env file_
```
API_KEY = "your_api_key"
SECRET = "Your_secret_key"
```

## _Run_
```
uvicorn api:app --reload
```
##### Then go to  http://127.0.0.1:8000/docs/

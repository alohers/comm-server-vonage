"""
FasAPI endpoints
"""
from fastapi import FastAPI, Response, status
from fastapi.middleware.cors import CORSMiddleware
import main as sv

app = FastAPI()

origins = [
    "http://localhost",
    "http://localhost:8080",
    "http://aloha-test.aloha.ar:2526"
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

@app.get("/new")
def createSession(mail: str, cliente: str, asesor: str):
    """
    Generates a new session and tokens
    """
    session = sv.newSession()
    tokenMod = sv.tokenMod(session)
    tokenCli = sv.tokenCli(session)
    id = sv.addSession(session.session_id, tokenCli, tokenMod, cliente, asesor)
    sv.sendMail(mail, id)
    return {
        "sessionId": session.session_id,
        "apiKey": session.sdk.api_key,
        "tokenMod": tokenMod,
        "tokenCli": tokenCli,
        "id": id,
        "cliente": cliente,
        "asesor": asesor
    }

@app.get("/testEnv")
def testEnv():
    """
    Test load_dotenv library
    """
    return{
        "API_KEY": sv.API_KEY,
        "SECRET": sv.SECRET
    }

@app.get("/testdb")
def testDB():
    """
    Check the sessions stored
    """
    return{
        "idLists": sv.idLists
    }

@app.get("/{item_id}", status_code=200)
def getId(item_id : str, response: Response):
    """
    Get the info from the id
    """
    if item_id in sv.idLists:
        return sv.idLists.get(item_id)
    else:
        response.status_code = 404
        return "Error: Key not found in DB"

from opentok import OpenTok
from opentok import Roles
from dotenv import load_dotenv
import os
import uuid


load_dotenv()

API_KEY = os.getenv('API_KEY')
SECRET = os.getenv('SECRET')

opentok = OpenTok(API_KEY, SECRET)

idLists = {}

def newSession():
    return opentok.create_session()


def tokenMod(session):
    """
    Generate moderator Token
    """
    return session.generate_token(role=Roles.moderator)


def tokenCli(session):
    """
    Generate client Token
    """
    return session.generate_token(role=Roles.publisher)


def sendMail(mail, id):
    """
    Send email to the Client
    """
    link = 'https://aloha-test.aloha.ar:2526/?code=%s' % id
    print("----------------------------------")
    print("MAIL=%s : LINK=%s " % (mail, link))
    print("----------- mail enviado --------")


def addSession(sessionId, tokenCli, tokenMod, cliente, asesor):
    """
    Add the session info to the dict
    """
    newId = uuid.uuid1()
    tempDict = {
        'sessionId': sessionId,
        'tokenMod': tokenMod,
        'tokenCli': tokenCli,
        'api_key': API_KEY,
        'cliente': cliente,
        'asesor': asesor
    }
    idLists[f'{newId}'] = tempDict
    return newId
